from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
import time
import sampling

class MLMetrics(object):
    def __init__(self, tp, tn, fp, fn):
        self.tp = tp
        self.tn = tn
        self.fp = fp
        self.fn = fn
        self.tpr = tp/(tp+fn)
        self.fpr = fp / (fp + tn)
        
    def __repr__(self):
        rval = "tp {0}, fp {1}, tn {2}, fn {3}, tpr {4}, fpr {5}".format(self.tp, self.fp, self.tn, self.fn, self.tpr, self.fpr)
        return rval
    
class AugmentedBias(object):
    def __init__(self, p, n):
        self.p = p
        self.n = n
        self.t = p + n
    
    def add(self, bias):
        self.p = bias.p + self.p
        self.n = bias.n + self.n
        self.t = bias.t + self.t

    def __repr__(self):
        return "p {0}, n {1}, t {2}".format(self.p, self.n, self.t)
    
def get_ml_metrics(train_y, train_pred):
    tn, fp, fn, tp = confusion_matrix(train_y, train_pred).ravel()
    metrics = MLMetrics(tp, tn, fp, fn)
    return metrics

def get_adjusted_count(metrics, y_test_pred, bias_adjustment=0):
    #print("TPR {0}, FPR {1}".format(metrics.tpr, metrics.fpr))
    if metrics.tpr == metrics.fpr:
        return sum(y_test_pred)
    #print("y_test_pred {0}, fpr: {1}, len y_test_pred {2}, tpr {3}".format(sum(y_test_pred), metrics.fpr, len(y_test_pred), metrics.tpr))
    return (sum(y_test_pred) - metrics.fpr * len(y_test_pred)) / (bias_adjustment + metrics.tpr - metrics.fpr)

def get_metrics_via_cross_val(X, y, clf, cv=10):
    y_pred = cross_val_predict(clf, X, y, cv=cv)
    return get_ml_metrics(y, y_pred)

def quantify_no_augment(raw_frame, fit, n=.1, label='label'):
    preaugment = []
    adjusted_preaugment = []

    #num_samples = int(n* len(raw_frame))
    num_samples = n
    runtimes = []
    tpr_dists = []
    fpr_dists = []
    for i in range(0, 100):
        start = time.time()
        #print("Iteration {0}".format(i))
        S = raw_frame.sample(n=num_samples)

        #reservoir = raw_frame.sample(n=num_samples)
        while not 1 in S[label].values:
            S = raw_frame.sample(n=num_samples)
        T = raw_frame.drop(S.index)
        clf = fit(S, label=label)

        metrics = get_metrics_via_cross_val(S[['x', 'y']], S[label], clf, cv=3)
        y_test_pred = clf.predict(T[['x', 'y']])
        full_metrics = get_ml_metrics(T[label], y_test_pred)
        tpr_dists.append(metrics.tpr - full_metrics.tpr)
        fpr_dists.append(metrics.fpr - full_metrics.fpr)

        ap = get_adjusted_count(metrics, y_test_pred)
        ap += sum(S[label])
        sample_count = sum(S[label])
        #y_cc = sum(y_test_pred)
        y_cc = sum(y_test_pred) + sample_count
        preaugment.append(y_cc)
        adjusted_preaugment.append(ap)
    return preaugment, adjusted_preaugment

def quantify_augment(raw_frame, fit, knn_search, n=.1, n2=500, label='label'):
    augment = []
    adjusted = []

    quantified_totals = []
    #num_samples = int(n * len(raw_frame))
    num_samples = n
    runtimes = []
    total_augments = n2
    batches = 20
    adjusted_metrics = []
  
    tpr_dists = []
    fpr_dists = []
    for i in range(0, 100):
        start = time.time()
        #print(i)
        if i % 50 == 0:
            print("Iteration {0}".format(i))
        S = raw_frame.sample(n=num_samples, replace=False)
        #reservoir = raw_frame.sample(n=num_samples)
        while not 1 in S[label].values:
            S = raw_frame.sample(n=num_samples, replace=False)

        batch_size = int(total_augments / batches)
        clf = fit(S, n=3, label=label)

        prev = S
        bias = None
        for i in range(1, batches):
            y_probs = clf.predict_proba(prev[['x', 'y']])
            S1, S1_bias = sampling.sample_n_from_knn(prev, raw_frame, y_probs, knn_search, n=batch_size, label=label)
            if not bias:
                bias = S1_bias
            else:
                bias.add(S1_bias)
            #print(len(S1.index.unique()))
            prev = S1
        clf = fit(S1, n=3, label=label)

        T = raw_frame.drop(S1.index)

        y_train_pred = clf.predict(S[['x', 'y']])
        metrics = get_metrics_via_cross_val(S[['x', 'y']], S[label], clf)

        y_test_pred = clf.predict(T[['x', 'y']])
        full_metrics = get_ml_metrics(T[label], y_test_pred)
        tpr_dists.append(metrics.tpr - full_metrics.tpr)
        fpr_dists.append(metrics.fpr - full_metrics.fpr)
        bias_adjustment = 0
        ap = get_adjusted_count(metrics, y_test_pred, bias_adjustment)
        adjusted_metrics.append(metrics)

        sample_count = sum(S1[label])
        #y_cc = sum(y_test_pred)
        y_cc = sum(y_test_pred) + sample_count
        ap += sample_count

        augment.append(y_cc)
        adjusted.append(ap)
    return augment, adjusted