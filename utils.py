label_map = {"label":"XS", "label_mid":"S", "label_large":"M", "label_large2":"L", "label_large3":"XL", "label_large4":"XXL"}
label_reverse_map = {v: k for k, v in label_map.items()}
labels = ["XS", "S", "M", "L", "XL", "XXL"]
kskyband_sizes = {"465":"1%", "932":"2%", "2330":"5%", "3729":"8%", "4662":"10%", "6994":"15%", "9326":"20%"}
distance_sizes = {"732":"1%", "1464":"2%", "3660":"5%", "5857":"8%", "7322":"10%", "10984":"15%", "14646":"20%"}

#distance_frame['size'] = distance_frame['label'].apply(lambda x: labels[x])
#kskyband_frame['size'] = kskyband_frame['label'].apply(lambda x: labels[x])

size_selections = ["1%", "2%", "10%"]
label_selections=["XS", "M", "XL"]
method_map = {"SRS":"RS", "SS":"SS", "BAWS":"LWS", "SSC":"LSS", "SSC-OS":"LSS-OS", "SSC-OS-NN3":"LSS-OS-NN3", "SSC-OS-NN1":"LSS-OS-NN1", "SSC-Static":"LSS-Static", "SSC-B":"LSS-EF", "SSC-Beta":"LSS-Beta"}

def bucket_array_histogram(bucket_array):
    #boundaries are in order from high to low
    histogram = []
    zeroes = []
    ones = []
    
    for bucket in bucket_array.rects:
        count_all = len(bucket.elements)
        count_zero = 0
        count_one = 0
        for elem in bucket.elements:
            if elem[-1] == 0:
                count_zero += 1
            else:
                count_one += 1
        histogram.append(len(bucket.elements))
        zeroes.append(count_zero)
        ones.append(count_one)
    return histogram, zeroes, ones

