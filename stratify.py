import itertools
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from collections import defaultdict
import time
import seaborn as sns
from sklearn.metrics import f1_score
from sklearn.metrics import brier_score_loss
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import auc
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix
from sklearn.utils import resample

from sklearn.model_selection import train_test_split


class Rect(object):
    def __init__(self, x0, y0, width, height):
        self.x = x0
        self.y = y0
        self.width = width
        self.height = height
        self.probability = 0
        self.sample_probability = 0
        self.count = -1
        self.exp_count = -1
        self.join_rate = -1
        self.lower_sample_range = 0
        self.upper_sample_range = 0
        self.elements = []
        self.outer_elements = []
        self.results = []
        self.rejects = []
        
        
    def std_dev(self):
        if not self.elements:
            return 0
        vals = [x[2] for x in self.elements]
        return np.std(vals)
    
    def contains(self, x1, y1):
        x = self.x
        y = self.y
        if x <= 0:
            x += -1
        if y <= 0:
            y += -1
        return x < x1 and y < y1 and self.x + self.width >= x1 and self.y + self.height >= y1
    
    def top_right(self):
        return (self.x + self.width, self.y + self.height)
    
    def bounding_box(self):
        return (self.x, self.y, self.x + self.width, self.y + self.height)
    
    def __repr__(self):
        return "(x={0}, y={1}, w={2}, h={3}, P={4:.2f})".format(self.x, self.y, self.width, self.height, self.probability)

    def equals(self, obj):
        if self.x == obj.x and self.y == self.y:
            return True
        return False

class Grid(object):
    def __init__(self, x, y, splitx, splity, xmin=0, ymin=0):
        self.rects = []
        self.rect_width = (x + abs(xmin))/splitx
        self.rect_height = (y + abs(ymin))/splity
        self.elements = []
        self.outer_elements = []
        for i in range(0, splitx):
            for j in range(0, splity):
                self.rects.append(Rect(xmin + i * self.rect_width, ymin + j * self.rect_height,
                                  self.rect_width, self.rect_height))
    def get_dominating_rects(self, r):
        dom_r = []
        corner = r.top_right()
        for rect in self.rects:
            if rect.x >= corner[0] and rect.y >= corner[1]:
                dom_r.append(rect)
        return dom_r
    
    def add(self, element):
        self.elements.append(element)
        added = False
        for r in self.rects:
            if r.contains(element[0], element[1]):
                r.elements.append(element)
                added = True
                return
        print("Nothing added {0} {1}".format(added, element))
            
    def add_outer(self, element):
        self.outer_elements.append(element)
        for r in self.rects:
            if r.contains(element[0], element[1]):
                r.outer_elements.append(element)
                
    def std_dev(self):
        vals = []
        for r in self.rects:
            vals.append((r, r.std_dev()))
        return vals
    
    def counts(self):
        for r in self.rects:
            print("R {0}, Size: {1}".format(str(r), len(r.elements)))
    
class Bucket(object):
    def __init__(self):
        self.elements = []
        self.probs = None
        self.preds = None
        
    
    def add(self, element):
        self.elements.append(element)
        
    def std_dev(self):
        if not self.elements:
            return 0
        vals = [x[2] for x in self.elements]
        return np.std(vals)
    
    def adj_std_dev(self):
        if not self.elements:
            return 0
        vals = [x[2] for x in self.elements]
        cnt = sum(vals)
        size = len(vals)
        p = (cnt + 2) / (size + 4)
        return math.sqrt(p * (1-p))
    
    def pred_std_dev(self):
        if not self.preds:
            return 0
        return np.std(self.preds)
    
class BucketArray(object):
    def __init__(self, boundaries=[.75, .5, .25, 0]):
        self.boundaries = boundaries
        self.rects = []
        self.elements = None
        
    def create_buckets(self, frame, clf, columns):
        Y_probs = clf.predict_proba(frame[['x', 'y']])
        Y_labels = clf.predict(frame[['x', 'y']])
        label_1_probs = Y_probs[:, 1]
        small_frame = frame[columns]
        self.elements = small_frame.values
        labeled_frame = small_frame.assign(probs=label_1_probs)
        labeled_frame = labeled_frame.assign(preds=Y_labels)
        prev = 1.01
        for p in self.boundaries:
            bucket = Bucket()
            probs_frame = labeled_frame[['probs', 'preds']]
            probs_frame_filter = probs_frame[(probs_frame.probs >= p) & (probs_frame.probs < prev)]
            bucket.elements = small_frame[(labeled_frame.probs >= p) & (labeled_frame.probs < prev)].values.tolist()
            bucket.probs = probs_frame_filter['probs'].values.tolist()
            bucket.preds = probs_frame_filter['preds'].tolist()
            self.rects.append(bucket)
            prev = p
        return self.rects
        
        
    """    full = frame[columns].values
        self.elements = full
        for elem, prob in zip(full, label_1_probs):
            for idx, q in enumerate(self.boundaries):
                if prob >= q:
                    self.rects[idx].add(elem)
                    break
        return self.rects
    """
    
    
    def std_dev(self):
        vals = []
        for b in self.rects:
            vals.append((b, b.std_dev()))
        return vals
    
    def pred_std_dev(self):
        vals = []
        for b in self.rects:
            vals.append((b, b.pred_std_dev()))
        
class FixedHeightBucketArray(object):
    def __init__(self, num_buckets=5):
        self.rects = []
        self.elements = None
        self.num_buckets = num_buckets
    
    def create_buckets(self, frame, clf, columns):
        self.elements = frame[columns]
        Y_probs = clf.predict_proba(frame[['x', 'y']])
        size = len(frame)
        num_elems_per_bucket = size / self.num_buckets
        labeled_frame = frame.assign(probs=Y_probs[:, 1])

        labeled_frame = labeled_frame.sort_values("probs", ascending=False)
        sub_frames = np.array_split(labeled_frame, self.num_buckets)
        for f in sub_frames:
            bucket = Bucket()
            bucket.elements = f[columns].values.tolist()
            self.rects.append(bucket)
        return self.rects
    
    def std_dev(self):
        vals = []
        for b in self.rects:
            vals.append((b, b.std_dev()))
        return vals
    
def get_strata_variance(N_h, n_h, sample_variance):
    if not n_h:
        return 0
    stratum_population_variance = (N_h * N_h) * (sample_variance / n_h) * ((N_h - n_h) / N_h)
    variance = stratum_population_variance
    if variance < 0:
        print("n_h {0}, N_h {1}, sample_variance {2}".format(n_h, N_h, sample_variance))
    return variance


#now calculate the standard deviation of each rectangle
#estimates = []
#variances = []
#first_stage_sample_size = 5000
#second_stage_sample_rate = .1

def normalize_probability(probabilities):
    total_probability = sum(x for x in probabilities)
    non_zero = 0
    for x in probabilities:
        if x > 0:
            non_zero += 1
    ratio = non_zero / len(probabilities)
    if not total_probability:
        norm_probs = [1/len(probabilities) for x in probabilities]
    else:
        norm_probs = [x / total_probability for x in probabilities]
    return norm_probs, total_probability, ratio

def get_nhs(full_stratum, n2):
    weighted_bucket_devs = []
    for bucket in full_stratum.rects:
        weighted_bucket_devs.append(len(bucket.elements) * bucket.std_dev())
        
    sum_bucket_devs = sum(weighted_bucket_devs)
    if sum_bucket_devs == 0:
        sum_bucket_devs = 1
    nhs = []
    for bucket in full_stratum.rects:
        ratio = (len(bucket.elements) * bucket.std_dev()) / sum_bucket_devs
        nhs.append(int(n2 * ratio))
        
    return nhs
    
def get_nhs_adj(full_stratum, n2):
    weighted_bucket_devs = []
    for bucket in full_stratum.rects:
        weighted_bucket_devs.append(len(bucket.elements) * bucket.adj_std_dev())
        
    sum_bucket_devs = sum(weighted_bucket_devs)
    if sum_bucket_devs == 0:
        sum_bucket_devs = 1
    nhs = []
    for bucket in full_stratum.rects:
        ratio = (len(bucket.elements) * bucket.adj_std_dev()) / sum_bucket_devs
        nhs.append(int(n2 * ratio))
        
    return nhs

def strata_estimate(labels, N_h):
    num_positives = sum(labels)
    total_samples = len(labels)
    
    #can happen if N_h == 1
    if total_samples == 0:
        return 0
    pop_total_nh = (num_positives * (1 / total_samples)) * N_h
    return pop_total_nh

def strata_variance(labels, N_h):
    if N_h == 0:
        return 0
    var = np.var(labels)    
    total_samples = len(labels)
    variance = get_strata_variance(N_h, total_samples, var)
    return variance

class StrataSample(object):
    def __init__(self, index, N_h, n_h, sample_frame, columns):
        self.index = index
        self.N_h = N_h
        self.n_h = n_h
        self.sample_frame = sample_frame
        self.columns = columns
        if self.N_h == 0 or self.n_h == 0:
            self.est_population = 0
            self.est_variance = 0
        else:
            labels = self.sample_frame[columns[2]]

            self.est_population = strata_estimate(labels, self.N_h)
            self.est_variance = strata_variance(labels, self.N_h)

            if self.n_h != len(self.sample_frame):
                self.n_h = len(self.sample_frame)
                print("{0} did not match {1}".format(self.n_h, self.sample_frame))
        
def get_estimate(T_h, sample_stratum, skip_index):
    T = 0
    for idx, strata in enumerate(sample_stratum):
        if idx == skip_index:
            continue
        T += strata.est_population
    T += T_h
    return T

def get_variance(V_h, sample_stratum, skip_index):
    V = 0
    for idx, strata in enumerate(sample_stratum):
        if idx == skip_index:
            continue
        V += strata.est_variance
    V += V_h
    return V

def strata_bootstrap_estimate(strata):
    N_h = strata.N_h
    labels = strata.sample_frame[strata.columns[2]]
    
    resampled_labels = resample(labels, n_samples=N_h)
    T_h = strata_estimate(resampled_labels, N_h)
    return T_h

    
def _do_sample(sample_stratum, full_stratum, columns, n2, observer_dict={}, allocation="optimum", observer_level=1, probability="empirical", strategy="normal"):
    
    empirical_probabilities = [x[1] for x in sample_stratum.std_dev()]
    real_probabilities = [x[1] for x in full_stratum.std_dev()]
    
    norm_empirical_probs, total_emp_prob, emp_ratio = normalize_probability(empirical_probabilities)
    norm_real_probs, total_real_prob, real_ratio = normalize_probability(real_probabilities)
    
    if probability == "empirical":
        new_probs = norm_empirical_probs
        total_prob = total_emp_prob
        prob_ratio = emp_ratio
    else:
        new_probs = norm_real_probs
        total_prob = total_real_prob
        prob_ratio = real_ratio
    
    N_hs = [len(r.elements) for r in full_stratum.rects]
    fallback = 0
    if emp_ratio < .1:
        fallback = 1
    if allocation == "optimum":
        n_hs = [int(p * n2) for p in new_probs]
        #print(n_hs)
        observer_dict["stratum_probs"] = new_probs
        n_hs = get_nhs(full_stratum, n2)
        
    elif allocation == "proportional":
        sample_proportion = n2 / len(full_stratum.elements)
        n_hs = [int(len(r.elements) * sample_proportion) for r in full_stratum.rects] 
    else:
        n_hs = [int(p * n2) for p in new_probs]
        
    if observer_level > 0:
        observer_dict["N_hs"] = N_hs
        observer_dict["pre_n_hs"] = n_hs
    
    t_estimates = []
    stratum_variances = []
    new_n_hs = []
    actual_samples = []
    ones = []
    sample_frame = []
    for n_h, rect, first_rect in zip(n_hs, full_stratum.rects, sample_stratum.rects):
        N_h = len(rect.elements)
        if not rect.elements:
            t_estimates.append(0)
            stratum_variances.append(0)
            new_n_hs.append(0)
            actual_samples.append(0)
            ones.append(0)
            continue
        temp_frame = pd.DataFrame(rect.elements, columns=columns)

        if n_h < 2:
            n_h = 5
        if n_h > len(temp_frame):
            N_h = len(temp_frame)
            n_h = len(temp_frame)
            labels = temp_frame
        else: 
            labels = temp_frame.sample(n=n_h)

        new_n_hs.append(n_h)
        actual_samples.append(len(labels))
        
        count = sum(labels[columns[2]])
        sample_frame.extend(labels.values.tolist())
        t_h = (count * (1 / len(labels))) * N_h
        
        t_estimates.append(t_h)

        var = np.var(labels[columns[2]])
        variance = get_strata_variance(N_h, n_h, var)
        ones.append(count) #count of the number of positive labels in this strata
        stratum_variances.append(variance)

    if observer_level > 0:
        observer_dict["post_n_hs"] = new_n_hs
        observer_dict["actual_n_h_samples"] = actual_samples
        observer_dict["empirical"] = norm_empirical_probs
        observer_dict["t_estimates"] = t_estimates
        observer_dict["ones"] = ones
        observer_dict["fallback"] = fallback
        observer_dict["empty_ratio"] = emp_ratio
        observer_dict["probs"] = new_probs
    if observer_level > 2:
        observer_dict["sample_frame"] = sample_frame

    
    return sum(t_estimates), sum(stratum_variances)


def sampling_round(raw_frame, columns, n1=5000, n2=5000, grid_x=5, grid_y=5, allocation="proportional"):
    x_max = raw_frame[columns[0]].max()
    y_max = raw_frame[columns[1]].max()

    x_min = raw_frame[columns[0]].min()
    y_min = raw_frame[columns[1]].min()
    
    sample_frame = raw_frame.sample(n=n1)
    #print(R)
    #now we have a sample of the inner table, construct a grid of the outer table

    grid = Grid(x_max, y_max, grid_x, grid_y, xmin=x_min, ymin=y_min)
    
    R = sample_frame[columns].values
    for elem in R:
        grid.add(elem)

    #print("Setting up full grid")
    full_grid = Grid(x_max, y_max, grid_x, grid_y, xmin=x_min, ymin=y_min)

    
    full = raw_frame[columns].values
    #print(full)
    for elem in full:
        full_grid.add(elem)

    estimate, variance = _do_sample(grid, full_grid, columns, n2, probability="empirical", allocation=allocation)
    return estimate, variance

def minimum_stratum_variance(S, raw_frame, clf, columns, n2):
    #make buckets in .10 increments
    buckets = np.array(list(range(90, 0, -10))) / 100
    
    #now take all combinations of buckets
    estimated_variances = []
    possible_solutions = itertools.combinations(buckets, 4)
    num_visited = 0
    for sol in possible_solutions:
        num_visited += 1
        sol = list(sol)
        sol.append(0)
        sample_buckets = BucketArray(sol)
        full_buckets = BucketArray(sol)
        
        sample_buckets.create_buckets(S, clf, columns)
        full_buckets.create_buckets(raw_frame, clf, columns)
        
        #calculate how many samples we should take for each grid
        std_devs = sample_buckets.std_dev()
        variances = [v[1] * v[1] for v in std_devs]
        total_probability = sum(x[1] for x in std_devs)
        #for b, dev in std_devs:
        #    print("Size: {0}, SD: {1}".format(len(b.elements), dev))
            
        #if there is no total probability, use proportional allocation
        method = "proportional"
        if total_probability == 0:
            sample_proportion = n2 / len(full_buckets.elements)
            n_hs = [int(len(r.elements) * sample_proportion) for r in full_buckets.rects]
        else:    
            new_probs = [x[1] / total_probability for x in std_devs]
            n_hs = [int(p * n2) for p in new_probs]
            method = "optimal"
            
        #now we might have more samples in a strata than the size of the strata, correct this
        
        N_hs = [len(r.elements) for r in full_buckets.rects]
        
        new_n_hs = []
        extra_samples = 0
        full_strata = 0
        for n_h, N_h in zip(n_hs, N_hs):
            if n_h > N_h:
                new_n_hs.append(N_h)
                extra_samples += n_h - N_h
                full_strata += 1
            else:
                new_n_hs.append(n_h)
        
        adjusted_n_hs = []
        distribution_per_strata = math.floor(extra_samples/(len(new_n_hs) - full_strata))
        for n_h, N_h in zip(new_n_hs, N_hs):
            allocation = 0
            if n_h != N_h:
                extra_space = N_h - n_h
                allocation = distribution_per_strata
                if allocation > extra_space:
                    allocation = extra_space
            adjusted_n_hs.append(n_h + allocation)
        n_hs = adjusted_n_hs
        est_var = 0
        for N_h, n_h, s_h in zip(N_hs, n_hs, variances):
            var = get_variance(N_h, n_h, s_h)
            if var < 0:
                print("method: {0}, n_hs {1}, N_hs {2}, n2 {3}".format(method, n_hs, N_hs, n2))
            est_var += var
        estimated_variances.append((est_var, sol))
        if est_var <= 0:
            print("Num Visited {0}".format(num_visited))
            return (est_var, sol)
    estimated_variances.sort(key=lambda x: x[0])
    print("Num Visited {0}".format(num_visited))
    return estimated_variances[0]
    
def test_minimum_stratum_variance(raw_frame, columns, fit, n1=1000, n2=5000, label="label"):
    x_max = raw_frame[columns[0]].max()
    y_max = raw_frame[columns[1]].max()

    S = raw_frame.sample(n=n1)
    while not 1 in S[label].values:
        S = raw_frame.sample(n=n1)
        #print(S[S['label'] == 1])
        #print(S)
        #take some samples
    clf = fit(S, n=3, label=label)
    return minimum_stratum_variance(S, raw_frame, clf, columns, n2)

def histogram(raw_frame, columns, fit, n1=5000, n2=5000, label="label"):
    x_max = raw_frame[columns[0]].max()
    y_max = raw_frame[columns[1]].max()

    S = raw_frame.sample(n=n1)
    while not 1 in S[label].values:
        S = raw_frame.sample(n=n1)
        #print(S[S['label'] == 1])
        #print(S)
        #take some samples
    clf = fit(S, n=3, label=label)
    y_sample_pred = clf.predict(S[['x', 'y']])
    y_sample_pred_probs = clf.predict_proba(S[['x', 'y']])[:, 1]
    
    y_full_pred = clf.predict(raw_frame[['x', 'y']])
    y_full_pred_probs = clf.predict_proba(raw_frame[['x', 'y']])[:, 1]
    return {"sample_true":S[label], "sample_pred":y_sample_pred, "sample_pred_probs":y_sample_pred_probs, "full_pred":y_full_pred, "full_pred_probs":y_full_pred_probs, "full_true":raw_frame[label]}

def sample_with_classifier(raw_frame, columns, fit, n1=5000, n2=5000, label="label", steps=[.9, .65, .45, .20, 0], allocation="optimum", return_bins=False, probability="empirical", folds=1, bucket_strategy="fixed_width", observer_level=1, strategy="jackknife"):
    
    x_max = raw_frame[columns[0]].max()
    y_max = raw_frame[columns[1]].max()

    S = raw_frame.sample(n=n1)
    while not 1 in S[label].values or len(S[S[label] == 1]) < 3 or len(S[S[label] == 0]) < 3:
        S = raw_frame.sample(n=n1)
        #print(S[S['label'] == 1])
        #print(S)
        #take some samples
        
    #now do the cross_val or k_fold or whatever.
    if folds == 1:
        train, test = train_test_split(S, test_size=0.2)
        while not 1 in train[label].values or not 1 in test[label].values or not 0 in train[label].values or not 0 in test[label].values:
            train, test = train_test_split(S, test_size=0.2)
        clf = fit(train, n=3, label=label)
    
    
    if not steps:
        est_var, steps = minimum_stratum_variance(test, raw_frame, clf, columns, n2)
    if bucket_strategy == "fixed_width":
        quadrants = steps
        full_buckets = BucketArray(quadrants)
        sample_buckets = BucketArray(quadrants)

        sample_buckets.create_buckets(test, clf, columns)
        full_buckets.create_buckets(raw_frame, clf, columns)
    else:
        #fixed height strategy
        full_buckets = FixedHeightBucketArray()
        sample_buckets = FixedHeightBucketArray()

        sample_buckets.create_buckets(test, clf, columns)
        full_buckets.create_buckets(raw_frame, clf, columns)                   
        
    #y_sample_pred = clf.predict(S[['x', 'y']])
    #y_sample_pred_probs = clf.predict_proba(S[['x', 'y']])
    
    scores_dict = {}

    y_pred_probs = clf.predict_proba(raw_frame[['x', 'y']])

    observer = {}
    if strategy == "jackknife" or strategy == "bootstrap":
        do_sample_func = _do_sample_jackknife
    else:
        do_sample_func = _do_sample
        
    if allocation =="adaptive":
        #run both and pick the best one
        estimate, variance = do_sample_func(sample_buckets, full_buckets, columns, n2, observer_dict=observer, allocation="proportional", probability=probability, observer_level=observer_level, strategy=strategy)
        
        observer_opt = {}
        estimate_opt, variance_opt = do_sample_func(sample_buckets, full_buckets, columns, n2, observer_dict=observer_opt, allocation="optimum", probability=probability, observer_level=observer_level, strategy=strategy)
        if variance_opt < variance:
            estimate = estimate_opt
            variance = variance_opt
            observer = observer_opt
        
    else:     
        estimate, variance = do_sample_func(sample_buckets, full_buckets, columns, n2, observer_dict=observer, allocation=allocation,  probability=probability, observer_level=observer_level, strategy=strategy)
        
    if return_bins:
        scores_dict["sample_bins"] = sample_buckets
        scores_dict["full_bins"] = full_buckets
    bucket_total_counts = []
    bucket_std_dev = []
    for bucket in full_buckets.rects:
        bucket_total_counts.append(len(bucket.elements))
        bucket_std_dev.append(bucket.std_dev())
    scores_dict["bucket_total_counts"] = bucket_total_counts
    scores_dict["bucket_std_dev"] = bucket_std_dev
    scores_dict["std_dev"] = bucket_std_dev
    scores_dict["observer"] = observer
    scores_dict["true_probs"] = y_pred_probs
    
    if "sample_frame" in observer:
        #produce accuracy and precision estimates for our first phase and second phase sample data
        sample_frame = np.array(observer["sample_frame"])
        #print(sample_frame)
        sample_x = sample_frame[:, :2]
        sample_y = sample_frame[:, -1]
        sample_f1 = f1_score(sample_y, clf.predict(sample_x))
        sample_brier = brier_score_loss(sample_y, clf.predict_proba(sample_x)[:, 1])
        scores_dict["sample_f1"] = sample_f1
        scores_dict["sample_brier"] = sample_brier
    return estimate, variance, scores_dict