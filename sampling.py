import numpy as np
import pandas as pd
import quantify
import time

class SampleData(object):
    def __init__(self, estimates, zs, runtimes, expectations, weighted_sample_sizes, variances=[]):
        self.estimates = estimates
        self.zs = zs
        self.runtimes = runtimes
        self.expectations = expectations
        self.weighted_sample_sizes = weighted_sample_sizes
        self.variances = variances
        
def srs(raw_frame, label='label', trials=100):
    sample_totals = []
    for i in range(0, 100):
        size_r = int(.10 * len(raw_frame))
        S = raw_frame.sample(n=size_r)
        cnts = len(S[S[label] == 1])
        N = len(raw_frame)
        sample_up_total = N/len(S) * cnts
        sample_totals.append(sample_up_total)
    return sample_totals

def sample_expectation(raw_frame, fit, knn_search, n=.1, n2=5000, label='label', trials=100):
    reduced_totals = []
    num_samples = int(n * len(raw_frame))
    num_estimate_samples = int(n2 * len(raw_frame))
    runtimes = []
    base_expectation = []
    all_base_zs = []
    sample_sizes = []
    reductions = []

    for i in range(0, trials):
        variance_removed = 0
        start = time.time()
        if i % 10 == 0:
            print("Iteration {0}".format(i))
        S = raw_frame.sample(n=num_samples)
        while not 1 in S[label].values:
            S = raw_frame.sample(n=num_samples)
        clf = fit(S, n=3, label=label)
        Y_probs = clf.predict_proba(raw_frame[['x', 'y']])
        label_1_probs = Y_probs[:, 1]
        Y_prob_total = sum(label_1_probs)
        base_expectation.append(Y_prob_total)
        closest_n = find_n_nearest(label_1_probs, 1.0, n=n2)
        for idx in closest_n:
            prob = label_1_probs[idx]
            row = raw_frame.iloc[idx]
            actual_value = row[label]
            Y_prob_total += actual_value - prob
            variance_removed += abs(actual_value - prob)
        reduced_totals.append(Y_prob_total)
        reductions.append(variance_removed)
    data = SampleData(reduced_totals, reductions, runtimes, base_expectation, sample_sizes)
    return data

def sample(raw_frame, fit, knn_search, n1=.1, n2=.1, augment=True, label='label', trials=100, **kwargs):
    weighted_totals = []
    #num_samples = int(n * len(raw_frame))
    #num_estimate_samples = int(n2 * len(raw_frame))
    num_samples = n1
    num_estimate_samples = n2
    runtimes = []
    base_expectation = []
    all_base_zs = []
    sample_sizes = []
    estimated_variances = []

    for i in range(0, trials):
        start = time.time()
        #if i % 10 == 0:
        #    print("Iteration {0}".format(i))
        S = raw_frame.sample(n=num_samples)
        while not 1 in S[label].values:
            S = raw_frame.sample(n=num_samples)
        #print(S[S['label'] == 1])
        #print(S)
        #take some samples
        clf = fit(S, n=3, label=label)

        #train the classifier
        if augment:
            total_augments = 500
            batches = 20
            batch_size = int(total_augments / batches)
            
            prev = S
            for i in range(1, batches):
                y_probs = clf.predict_proba(prev[['x', 'y']])
                S1, S1_bias = sample_n_from_knn(prev, raw_frame, y_probs, knn_search, n=batch_size, label=label)
                prev = S1
                clf = fit(S1, n=3, label=label)

        #we know the exact answers for S1 values, so update accordingly
        Y_probs = clf.predict_proba(raw_frame[['x', 'y']])
        label_1_probs = Y_probs[:, 1]
        Y_prob_total = sum(label_1_probs)
        base_expectation.append(Y_prob_total)

        raw_frame['prob'] = label_1_probs / Y_prob_total
        max_samples = len(raw_frame[raw_frame['prob'] > 0])
        if max_samples < num_estimate_samples:
            num_estimate_samples = max_samples
        sample_sizes.append(num_estimate_samples)
        weighted_samples = raw_frame.sample(n=num_estimate_samples, weights=label_1_probs)

        ys = weighted_samples[label]
        ps = weighted_samples['prob'] 
        end = time.time()
        runtimes.append(end-start)

        N = len(raw_frame)
        
        include_zero = False
        if "include_zero" in kwargs:
            include_zero = kwargs["include_zero"]
        base_pop_total, base_zs = orderedProbTotal(ys, ps, N, Y_prob_total, include_zero=include_zero)

        all_base_zs.append(base_zs)
        weighted_totals.append(base_pop_total)
        estimated_variances.append(orderedProbVariance(base_zs, np.mean(base_zs)))
    data = SampleData(weighted_totals, all_base_zs, runtimes, base_expectation, sample_sizes, variances=estimated_variances)
    return data

def calc_prob_ranges(Y_prob, set_zero=None, sample_set=None):
    if not set_zero is None:
        Y_prob[set_zero] = 0
    if sample_set:
        for j in sample_set:
            Y_prob[j] = 0
    Y_total = sum(Y_prob)
    if Y_total == 0:
        return []
    Y_probs_norm = Y_prob / Y_total
    cumulative = 0
    prob_ranges = []
    for p in Y_probs_norm:
        p_new = cumulative + p
        cumulative += p
        prob_ranges.append(p_new)
    return prob_ranges

def orderedProbMean(ys, ps, N):
    z_bar = 0
    cum_y = 0
    cum_p = 1
    zs = []
    for y, p in zip(ys, ps):
        z = (1/N) * (cum_y + (y/p)*(cum_p))
        print(z)
        cum_y += y
        cum_p -= p
        zs.append(z)
    z_bar = (1 / len(ys)) * sum(zs)
    #print(zs)
    return z_bar, zs

def orderedProbTotal(ys, ps, N, Y_prob_total, include_zero=False):
    z_bar = 0
    cum_y = 0
    cum_p = 1
    zs = []
    leftover_p = 0
    for y, p in zip(ys, ps):
        if y == 0:
            leftover_p += p * Y_prob_total
    
    #print("Leftover p {0}".format(leftover_p))
    #print("Y_prob_total {0}".format(Y_prob_total))
    excluded = 0
    for y, p in zip(ys, ps):
        if y == 0 and not include_zero:
            excluded += 1
            continue
        
        #new_p = (p * Y_prob_total) / (Y_prob_total - leftover_p)
        new_p = p
        z = (cum_y + (y/new_p)*(cum_p))
        cum_y += y
        cum_p -= new_p
        #print("y {0}, p {1}, cum_y {2}, cum_p {3}, z {4}".format(y, p, cum_y, cum_p, z))
        
        zs.append(z)
    z_bar = (1 / (len(ys) - excluded)) * sum(zs)
    return z_bar, zs

def orderedProbVariance(zs, z_bar):
    variance = 0
    denom = len(zs) * (len(zs) - 1)
    for z in zs:
        variance += (z - z_bar)**2
    return (1/denom) * variance
    
def get_sampling_probability(prob_ranges, i):
    if i == 0:
        return prob_ranges[i]
    else:
        return prob_ranges[i] - prob_ranges[i-1]
    
### utilities for searching nearest neighbors
def find_n_nearest(array, value, n=10):
    abs_values = np.abs(array - value)
    idxs = np.argpartition(abs_values, n, axis=None)
    return idxs[:n]

def search_knn(knn_search, x, frame, exclude=[], new_index=[]):
    #we expect a single dimensional array
    distances, indices = knn_search.kneighbors([x])
    distances = distances[0]
    indices = indices[0]
    idx_val = 0
    #or frame.iloc[idx_val].index.name in exclude or idx_val in new_index:
    while distances[idx_val] == 0 or indices[idx_val] in new_index or frame.iloc[indices[idx_val]].name in exclude:
        #print(frame.iloc[indices[idx_val]].name in exclude)
        idx_val += 1
        #if we are over the array, just return the second item
        if idx_val + 1 >= len(distances):
            #print("Distance {0}".format(distances[idx_val]))
            idx_val == 1
            break
    #print(idx_val)
    #print("Returned index {0}".format(indices[idx_val]))
    return indices[idx_val]

def sample_n_from_knn(S, frame, y_probs, knn_search, n = 10, label='label'):
    closest_n = find_n_nearest(y_probs[:, 1], .5, n=n)
    indices = []
    
    for idx in closest_n:
        row = S.iloc[idx]
        idx = search_knn(knn_search, row[['x', 'y']], frame, exclude=S.index, new_index=indices)
        indices.append(idx)
    #print(len(set(indices)))
    samples = frame.iloc[indices]
    p = sum(samples[label] == 1)
    n = sum(samples[label] == 0)
    bias = quantify.AugmentedBias(p, n)
    sample_frame = pd.concat([samples, S])
    return sample_frame, bias

def mean_and_diff(zs_array, clip=200):
    trimmed = []
    for z in zs_array:
        trimmed.append(z[0:clip])
    all_zs = np.array(trimmed)

    #print(all_zs.shape)
    zs_diffs = []

    for index, zs in enumerate(all_zs):
        prev = 0
        z_sub = []
        for j, z_val in enumerate(zs):
            if j == 0:
                z_sub.append(0)
            else:
                z_sub.append(abs(z_val - prev))
            prev = z_val
        zs_diffs.append(z_sub)

    #now take the column mean
    zs_diffs = np.array(zs_diffs)

    zs_cols = np.mean(zs_diffs, axis=0)
    zs_std = np.std(all_zs, axis=0)
    #print(zs_cols)
    zs_mean = np.mean(all_zs, axis=0)
    return zs_mean, zs_cols, zs_std
    